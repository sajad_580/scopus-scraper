from selenium import webdriver
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from difflib import SequenceMatcher
from sqlalchemy import or_
import json
import time
import datetime
import random

from app import db, ScraperSession, FailedArticle, Author
from config import config, xpath
from options import export_preparation


def my_print(type):
    if type == 1:
        print("")
    else:
        print(
            "======================================================================================"
        )

    return


urls = open("urls.txt").read()
start_url = list(filter(None, urls.split("\n")))


if len(start_url) == 0:
    my_print(1)
    print("============= The url is required =============")
    my_print(1)
else:
    authentication_wait_time = int(config("AUTHENTICATION_WAIT_TIME"))
    parse_author_articles = int(config("PARSE_AUTHOR_ARTICLES"))
    matching_percentage = float(config("MATCHING_PERCENTAGE"))
    seperator = config("SEPERATOR")
    parse_type_3_authors = config("PARSE_TYPE_3_AUTHORS")
    articles_info = []
    checked_articles = []
    checked_authors = []

    scraper_type = int(config("SCRAPER_TYPE"))
    scraper_type_input = input(
        f"Please enter scraper type ( default is {scraper_type} ): "
    )
    if len(scraper_type_input) > 0:
        scraper_type = int(scraper_type_input)

    db.session.add(
        ScraperSession(
            url=seperator.join(start_url),
            last_checked_url="",
            type=scraper_type,
            created_date=datetime.datetime.now(),
        )
    )
    db.session.commit()

    scraper_session = (
        db.session.query(ScraperSession).order_by(ScraperSession.id.desc()).first()
    )

    my_print(1)
    my_print(1)
    my_print(2)
    print(
        f"Scraper session created. You can find the results with this session_id: ( {scraper_session.id} )"
    )
    my_print(2)
    my_print(1)
    my_print(1)

    session_id = scraper_session.id

    chrome_options = Options()
    chrome_options.add_argument("--disable-gpu")
    chrome_options.add_experimental_option("excludeSwitches", ["enable-logging"])
    driver = webdriver.Chrome(
        # service=Service(ChromeDriverManager().install()),
        service=Service("./chromedriver.exe"),
        options=chrome_options,
    )
    driver.maximize_window()

    action = ActionChains(driver)


def init():
    for i, url in enumerate(start_url):
        scraper_session.last_checked_url = url
        db.session.commit()

        driver.get(url)
        if i == 0:
            time.sleep(authentication_wait_time)

        if scraper_type == 1:
            get_article_detail()
            get_article_authors()
            get_author()

        elif scraper_type == 2:
            parse_author(i)

    if parse_type_3_authors:
        get_type_3_authors_detail()

    authors = (
        db.session.query(Author)
        .filter(
            Author.scraper_session_id == session_id,
            or_(
                Author.documents == "",
                Author.citations == "",
                Author.h_index == "",
                Author.affiliation_history == "",
            ),
        )
        .all()
    )

    if authors is not None:
        update_authors_info(authors)

    driver.close()
    export_preparation(scraper_session)


def get_article_detail(return_data=False):
    bread_cromb_row = None

    time.sleep(random.uniform(0.03, 0.15))

    for i in range(2):
        try:
            bread_cromb_row = driver.find_element(
                by=By.XPATH, value=xpath("article_bread_cromb_row")
            ).find_elements(by=By.TAG_NAME, value="els-typography")
        except:
            driver.refresh()

    if bread_cromb_row:
        journal = bread_cromb_row[0].text
        title = driver.find_element(
            by=By.CSS_SELECTOR, value="span[class='Highlight-module__1p2SO']"
        ).text
        href = driver.current_url
        year = ""

        try:
            for item in bread_cromb_row:
                text = item.text.rsplit(" ", 1)
                text = text[-1] if len(text) > 1 else text[0]

                if (
                    len(year) == 0
                    and len(text) == 4
                    and (text.startswith("19") or text.startswith("20"))
                ):
                    year = text
        except:
            pass

        if not return_data:
            articles_info.append(
                {
                    "journal": journal,
                    "title": title,
                    "year": year,
                    "url": href,
                }
            )

            return
        else:
            return {
                "journal": journal,
                "title": title,
                "year": year,
                "url": href,
            }

    else:
        db.session.add(
            FailedArticle(scraper_session_id=session_id, url=driver.current_url)
        )
        db.session.commit()

        if return_data:
            return None
        else:
            return


def get_article_authors():
    for article in articles_info:
        if article["url"] not in checked_articles:
            checked_articles.append(article["url"])
            time.sleep(random.uniform(0.03, 0.15))
            driver.get(article["url"])
            time.sleep(random.uniform(0.03, 0.15))
            parse_article_authors(article)

    return


def parse_article_authors(article):

    article_is_valid = False
    authors = []

    for i in range(2):
        try:
            authors = driver.find_element(
                by=By.CSS_SELECTOR, value="ul[class='ul--horizontal margin-size-0']"
            ).find_elements(by=By.TAG_NAME, value="li")
        except:
            driver.refresh()

    for author in authors:
        try:
            author.find_element(by=By.TAG_NAME, value="els-button")
        except:
            driver.refresh()
            try:
                author.find_element(by=By.TAG_NAME, value="els-button")
            except:
                return

        name = author.text.split("\n")[0]
        url = ""
        email = ""
        author_type = 2

        time.sleep(random.uniform(0.03, 0.15))

        try:
            author.click()
        except:
            driver.refresh()
            try:
                author.click()
            except:
                url = None

        if url is not None:
            time.sleep(random.uniform(0.03, 0.15))
            try:
                links = driver.find_elements(
                    by=By.CSS_SELECTOR, value="els-button[variant='link-alt']"
                )
            except:
                links = []

            for link in links:
                try:
                    href = link.get_attribute("href")
                except:
                    href = None

                if href:
                    if href.startswith("/authid/detail"):
                        url = f"https://www.scopus.com{href}"

                    elif href.startswith("mailto"):
                        author_type = 1
                        email = href.split(":")[1]

        if url is not None and len(url) > 0:
            article_is_valid = True

            db.session.add(
                Author(
                    scraper_session_id=session_id,
                    name=name,
                    type=author_type,
                    email=email,
                    other_emails="",
                    url=url,
                    affiliation_history="",
                    documents="",
                    citations="",
                    h_index="",
                    root_article_journal=article["journal"],
                    root_article_title=article["title"],
                    root_article_year=article["year"],
                    root_article_url=article["url"],
                )
            )
            db.session.commit()

        action.send_keys(Keys.ESCAPE).perform()

    if not article_is_valid:
        db.session.add(FailedArticle(scraper_session_id=session_id, url=article["url"]))
        db.session.commit()

    return


def get_author():

    authors = (
        db.session.query(Author).filter(Author.scraper_session_id == session_id).all()
    )

    for index, author in enumerate(authors):
        if author.url not in checked_authors and int(author.type) != 3:
            checked_authors.append(author.url)
            time.sleep(random.uniform(0.03, 0.15))
            driver.get(author.url)
            time.sleep(random.uniform(0.03, 0.15))
            parse_author(index, author)

    return


def parse_author(index, db_author=None):
    if db_author is None:
        for i in range(2):
            try:
                name = driver.find_element(
                    by=By.CSS_SELECTOR,
                    value="h2[class='AuthorHeader-module__syvlN margin-size-4-t']",
                ).text
            except:
                if i < 2:
                    driver.refresh()
                    time.sleep(0.03)
                else:
                    name = ""
    else:
        name = db_author.name

    metrics_row = get_metrics_row()

    if metrics_row is not None:
        documents = metrics_row[0].text
        citations = metrics_row[1].text
        h_index = metrics_row[2].text
    else:
        documents = ""
        citations = ""
        h_index = ""

    affiliation_history = get_affiliation_history()

    driver.back()
    driver.refresh()
    time.sleep(random.uniform(0.03, 0.15))

    if index == 0:
        try:
            per_page_items_btn = driver.find_element(
                by=By.CSS_SELECTOR, value="nav[aria-label='pagination']"
            ).find_elements(by=By.TAG_NAME, value="option")

            per_page_items_btn[-1].click()
            driver.refresh()
            time.sleep(0.07)
        except:
            driver.refresh()

            try:
                per_page_items_btn = driver.find_element(
                    by=By.CSS_SELECTOR, value="nav[aria-label='pagination']"
                ).find_elements(by=By.TAG_NAME, value="option")

                per_page_items_btn[-1].click()
                driver.refresh()
                time.sleep(0.1)
            except:
                pass

    try:
        author_articles = driver.find_element(
            by=By.CSS_SELECTOR, value="ul[class='ViewType-module_resultsList__1d9Ed']"
        ).find_elements(by=By.CSS_SELECTOR, value="a[class='list-title']")
    except:
        author_articles = []

    author_articles_url = [item.get_attribute("href") for item in author_articles]

    other_emails = []
    author_article_info = None
    for i, url in enumerate(author_articles_url):
        if i <= parse_author_articles:
            driver.get(url)
            time.sleep(random.uniform(0.03, 0.15))
            result = get_author_emails()

            if result is not None:
                for author in result["authors"]:
                    if (
                        SequenceMatcher(None, name, author["name"]).ratio()
                        >= matching_percentage
                        and author["email"] not in other_emails
                    ):
                        other_emails.append(author["email"])
                        if db_author is None and author_article_info is None:
                            author_article_info = result["article"]
                    elif (
                        SequenceMatcher(None, name, author["name"]).ratio()
                        < matching_percentage
                        and parse_type_3_authors
                    ):

                        db_searched_author = (
                            db.session.query(Author)
                            .filter(
                                Author.scraper_session_id == session_id,
                                Author.type == 3,
                                or_(
                                    Author.name == author["name"],
                                    Author.url == author["url"],
                                ),
                            )
                            .first()
                        )

                        if db_searched_author:
                            db_author_emails = db_searched_author.other_emails.split(
                                seperator
                            )

                            try:
                                db_author_emails.remove("")
                            except:
                                pass

                            if author["email"] not in db_author_emails:
                                db_author_emails.append(author["email"])

                            db_searched_author.other_emails = seperator.join(
                                db_author_emails
                            )
                            db.session.commit()

                        else:
                            db.session.add(
                                Author(
                                    scraper_session_id=session_id,
                                    name=author["name"],
                                    type=3,
                                    email="",
                                    other_emails=author["email"],
                                    url=author["url"],
                                    affiliation_history="",
                                    documents="",
                                    citations="",
                                    h_index="",
                                    root_article_journal=result["article"]["journal"],
                                    root_article_title=result["article"]["title"],
                                    root_article_year=result["article"]["year"],
                                    root_article_url=result["article"]["url"],
                                )
                            )
                            db.session.commit()

    if db_author is not None:
        db_author.other_emails = seperator.join(other_emails)
        db_author.affiliation_history = seperator.join(affiliation_history)
        db_author.documents = documents
        db_author.citations = citations
        db_author.h_index = h_index
        db.session.commit()

    else:
        if author_article_info is not None:
            db.session.add(
                Author(
                    scraper_session_id=session_id,
                    name=name,
                    type=2,
                    email="",
                    other_emails=seperator.join(other_emails),
                    url=start_url[index],
                    affiliation_history=seperator.join(affiliation_history),
                    documents=documents,
                    citations=citations,
                    h_index=h_index,
                    root_article_journal=author_article_info["journal"],
                    root_article_title=author_article_info["title"],
                    root_article_year=author_article_info["year"],
                    root_article_url=author_article_info["url"],
                )
            )
            db.session.commit()

    return


def get_author_emails():

    try:
        authors = driver.find_element(
            by=By.CSS_SELECTOR, value="ul[class='ul--horizontal margin-size-0']"
        ).find_elements(by=By.TAG_NAME, value="li")
    except:
        driver.refresh()
        try:
            authors = driver.find_element(
                by=By.CSS_SELECTOR, value="ul[class='ul--horizontal margin-size-0']"
            ).find_elements(by=By.TAG_NAME, value="li")
        except:
            authors = []

    e_authors_info = []
    for author in authors:
        try:
            author.find_element(by=By.TAG_NAME, value="els-button")
        except:
            try:
                driver.refresh()
                author.find_element(by=By.TAG_NAME, value="els-button")
            except:
                pass

        try:
            email = (
                author.find_element(
                    by=By.CSS_SELECTOR,
                    value="els-button[variant='icon-colored-primary']",
                )
                .get_attribute("href")
                .split(":")[1]
            )
        except:
            email = None

        if email is not None:
            try:
                name = author.text.split("\n")[0]
            except:
                name = None

            url = ""

            if name:
                try:
                    time.sleep(random.uniform(0.03, 0.15))
                    author.click()
                    links = driver.find_elements(
                        by=By.CSS_SELECTOR, value="els-button[variant='link-alt']"
                    )
                    for link in links:
                        href = link.get_attribute("href")
                        if href and href.startswith("/authid/detail"):
                            url = f"https://www.scopus.com{href}"

                    e_authors_info.append(
                        {
                            "name": name,
                            "email": email,
                            "url": url,
                        }
                    )
                    action.send_keys(Keys.ESCAPE).perform()
                except:
                    pass

    if len(e_authors_info) > 0:
        article = get_article_detail(True)

        if article:
            return {
                "article": article,
                "authors": e_authors_info,
            }
        else:
            return None
    else:
        return None


def get_metrics_row():
    metrics_row = None

    for i in range(2):
        time.sleep(random.uniform(0.03, 0.15))
        try:
            metrics_row = driver.find_element(
                by=By.CSS_SELECTOR, value="div[class='col-lg-6 col-24']"
            ).find_elements(by=By.TAG_NAME, value="h3")

        except:
            if i < 2:
                driver.refresh()
            else:
                pass

    if metrics_row is None or len(metrics_row) != 3:
        driver.refresh()
        time.sleep(0.2)
        try:
            metrics_row = driver.find_element(
                by=By.CSS_SELECTOR, value="div[class='col-lg-6 col-24']"
            ).find_elements(by=By.TAG_NAME, value="h3")

        except:
            metrics_row = None

    if metrics_row is not None and len(metrics_row) == 3:
        return metrics_row
    else:
        return None


def get_affiliation_history():
    author_id = driver.current_url.split("=")[1].split("&")[0]
    url = (
        f"https://www.scopus.com/api/published-affiliation-history?authorId={author_id}"
    )
    driver.get(url)
    affiliations = json.loads(driver.find_element(by=By.TAG_NAME, value="body").text)
    return [
        f"{af['startYear']} - {af['endYear']} {af['affiliatedInstitution']['name']}, {af['affiliatedInstitution']['address']['city']}, {af['affiliatedInstitution']['address']['country']}"
        for af in affiliations
    ]


def get_type_3_authors_detail():

    authors = (
        db.session.query(Author)
        .filter(
            Author.scraper_session_id == session_id,
            Author.type == 3,
            or_(
                Author.documents == "",
                Author.citations == "",
                Author.h_index == "",
                Author.affiliation_history == "",
            ),
        )
        .all()
    )

    if authors is not None:
        update_authors_info(authors)

    return


def update_authors_info(authors):
    for author in authors:
        if (
            author.url
            and len(author.url) > 0
            and (
                int(author.type) != 3
                or (int(author.type) == 3 and parse_type_3_authors)
            )
        ):
            driver.get(author.url)
            time.sleep(random.uniform(0.03, 0.15))
            metrics_row = get_metrics_row()

            if metrics_row is not None:
                author.documents = metrics_row[0].text
                author.citations = metrics_row[1].text
                author.h_index = metrics_row[2].text
                db.session.commit()

            author.affiliation_history = seperator.join(get_affiliation_history())
            db.session.commit()

    return


if len(start_url) > 0:
    init()
