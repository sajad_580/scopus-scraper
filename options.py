from app import db, ScraperSession, FailedArticle, Author
import pandas as pd
from config import config


def main():
    print("")
    option_type = input("Please select option type ( 1 => Export | 2 => Delete ): ")
    print("")

    if option_type == "1":
        session_id = input(
            "Please enter session_id ( if you ignore it, it returns the data with last session_id ): "
        )

        if len(session_id) == 0:
            scraper_session = (
                db.session.query(ScraperSession)
                .order_by(ScraperSession.id.desc())
                .first()
            )
        else:
            scraper_session = (
                db.session.query(ScraperSession)
                .filter(ScraperSession.id == session_id)
                .first()
            )

        if scraper_session:
            export_preparation(scraper_session, True)
        else:
            print("")
            print("============== scraper session not found! ==============")
            print("")

    elif option_type == "2":
        confirm = input("Are you sure you want to clear the database? ( y / n ): ")

        if confirm == "y" or confirm == "yes":
            db.session.query(ScraperSession).delete()
            db.session.query(FailedArticle).delete()
            db.session.query(Author).delete()
            db.session.commit()

            print("")
            print("===================================")
            print("All data deleted!")
            print("===================================")
            print("")


def export_preparation(scraper_session, manual=False):

    authors = (
        db.session.query(Author)
        .filter(Author.scraper_session_id == scraper_session.id)
        .all()
    )

    if len(authors) > 0:

        data = []
        for i, author in enumerate(authors):
            data.append(
                (
                    author.name,
                    author.type,
                    author.email,
                    author.other_emails,
                    author.url,
                    author.affiliation_history,
                    author.documents,
                    author.citations,
                    author.h_index,
                    author.root_article_journal,
                    author.root_article_title,
                    author.root_article_year,
                    author.root_article_url,
                    scraper_session.last_checked_url if i == 0 else "---",
                )
            )

        name = f"authors__{scraper_session.created_date.strftime('%Y-%m-%d__%H-%M')}"

        if manual:
            name = name + "__manual__export"

        export(data, name, config("AUTHOR_HEADERS"))
        print_row(name)

    failed_articles = (
        db.session.query(FailedArticle)
        .filter(FailedArticle.scraper_session_id == scraper_session.id)
        .all()
    )

    if len(failed_articles) > 0:

        data = []
        for article in failed_articles:
            data.append((article.url,))

        name = f"failed__articles__{scraper_session.created_date.strftime('%Y-%m-%d__%H-%M')}"

        if manual:
            name = name + "__manual__export"

        export(data, name, ["Url"])
        print_row(name)


def export(data, name, header):

    df = pd.DataFrame(data)
    writer = pd.ExcelWriter(f"export/{name}.xlsx", engine="openpyxl")
    df.to_excel(writer, sheet_name="authors", index=False, header=header)
    writer.save()


def print_row(name):
    print("")
    print("===================================")
    print(f"{name} exported!")
    print("===================================")
    print("")


if __name__ == "__main__":
    main()
