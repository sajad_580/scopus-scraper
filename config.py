def config(key):

    switcher = {
        # you have to install python version 3.8 or 3.9 / pip package
        # you have to install packages in requirements.txt file with this command ==> pip install -r requirements.txt
        # you have to use vpn when you are in iran ( If you got an error at the start of the app )
        # ...
        # Type should be between 1 - 2 ( type 1 => search for one article / type 2 => search for one author)
        "SCRAPER_TYPE": 1,
        # ...
        # Waiting time to enter proxy credentials in the browser ( seconds )
        "AUTHENTICATION_WAIT_TIME": 60,
        # ...
        # Number of authors articles to be processed ( the program will only open this number of articles in each author profile )
        "PARSE_AUTHOR_ARTICLES": 30,
        # ...
        # Matching the author's name in different articles ( between 0.10 - 0.99 ) ( If this number is close to 0.99, it is difficult to match the author name )
        "MATCHING_PERCENTAGE": 0.40,
        # ...
        # Seperator of url and data in excel file ( this seperator is importand if we have multiple urls or emails, so we can have multiple emails for single author )
        "SEPERATOR": "; ",
        # ...
        # Get type 3 authors details ( True / False )
        "PARSE_TYPE_3_AUTHORS": True,
        # ...
        # author header in excel file
        "AUTHOR_HEADERS": [
            "Name",
            "Type",
            "Email",
            "Other Emails",
            "Url",
            "Affiliation History",
            "Documents",
            "Citations",
            "H-Index",
            "Article Journal",
            "Article Title",
            "Article Year",
            "Article Url",
            "Last Checked Url",
        ],
        # ...
        # proxy credentials
        "USER": 10578497,
        "PASSWORD": "gOgjZv2f",
    }

    return switcher.get(key, "")


def xpath(key):

    switcher = {
        "article_bread_cromb_row": "/html/body/div/div/div/div[2]/div/div[3]/div[3]/div/div/div[2]/micro-ui/scopus-document-details-page/div/article/div/div/div",
    }

    return switcher.get(key, "")
