from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///db.sqlite3"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(app)


class ScraperSession(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.String)
    last_checked_url = db.Column(db.String)
    type = db.Column(db.String)
    created_date = db.Column(db.DateTime)


class FailedArticle(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    scraper_session_id = db.Column(db.Integer)
    url = db.Column(db.String)


class Author(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    scraper_session_id = db.Column(db.Integer)
    name = db.Column(db.String)
    type = db.Column(db.String)
    email = db.Column(db.String)
    other_emails = db.Column(db.String)
    url = db.Column(db.String)
    affiliation_history = db.Column(db.String)
    documents = db.Column(db.String)
    citations = db.Column(db.String)
    h_index = db.Column(db.String)
    root_article_journal = db.Column(db.String)
    root_article_title = db.Column(db.String)
    root_article_year = db.Column(db.String)
    root_article_url = db.Column(db.String)
